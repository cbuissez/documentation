### Pour la procédure
lire [Readme du produit](README_DOCUSAURUS.md)

### Visualiser le rendu 
[EleaDoc](https://bertrandchartier.gitlab.io/eleadoc)

### On en parle
[FORUM Docusaurus IAM](https://iam.unistra.fr/mod/forum/view.php?id=4144)