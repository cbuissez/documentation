# Se connecter à Éléa en cas de fermeture de l'ENT 

<iframe title="e-éducation &amp; Éléa - Se connecter à Éléa en cas de fermeture de l'ENT" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/997b2a69-ff4c-4962-ba5c-14c9f239135f" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Vous vous êtes connecté à votre ENT, puis vous avez créé un parcours sur la plateforme Éléa.

![Se connecter à Éléa par l'ENT](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture01connexionviaent.png)

Que faire si votre ENT est temporairement inaccessible, pour des raisons de maintenance par exemple, ou de mise à jour et fermeture en été, ce qui vous prive d'accès à vos parcours Éléa ?

![Blocage par l'ENT fermé](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture02connexionimpossibleentferme.png)

Ce tutoriel va vous permettre de vous créer, grâce à vos **identifiants académiques** un accès alternatif à la plateforme Éléa, puis à votre parcours.

![Se connecter à Éléa avec ses identifiants académiques](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture03accesacademique.png)

La procédure à suivre exige les deux prérequis suivants.


## Prérequis n°1 : Vos identifiants académiques

Munissez-vous de vos identifiants académiques. 

Il s'agit des identifiants que vous employez déjà pour vous connecter au portail [Arena](https://id.ac-versailles.fr/login/ct_logon_mixte.jsp) de l'académie de Versailles, ou à [votre boîte mail académique](https://messagerie.ac-versailles.fr/iwc_static/layout/main.html?lang=fr&3.0.1.2.0_16020221) par exemple.

Généralement vos identifiants se présentent ainsi :

1. Nom utilisateur : initiale de votre prénom accolée à votre nom de famille, le tout en lettres minuscules ;

**Exemple** : une collègue qui se nommerait Christiane DUPONT aurait comme nom d'utilisateur académique cdupont. 

2. Mot de Passe : par défaut il s'agit de votre **numen** (sauf si vous avez déjà modifié celui-ci, pour un mot de passe de votre choix sur un service académique - portail [Arena](https://id.ac-versailles.fr/login/ct_logon_mixte.jsp) ou [boîte mail académique](https://messagerie.ac-versailles.fr/iwc_static/layout/main.html?lang=fr&3.0.1.2.0_16020221)).

## Prérequis n°2 : Identifier la plateforme Éléa de votre bassin d'éducation

L'académie est divisée en **24 bassins d'éducation** et chacun dispose d'une plateforme Éléa qui lui est propre. 

Pour connaître le bassin d'éducation auquel est rattaché votre établissement, après vous être connecté à Éléa via votre ENT, vous pouvez observer l'url qui s'affiche dans la barre d'adresse de votre navigateur internet.  Cette url est de la forme : **https://*BASSIN*.elea.ac-versailles.fr**.

**Exemple** : pour un établissement situé dans le bassin de la ville de Boulogne, l'adresse de la plateforme de bassin sera :

https://boulogne.elea.ac-versailles.fr

![SeconnecteraEleaencasdefermeture05repergaebassin](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture04repergaebassin.png)

**Remarque** : si vous ne connaissez pas le bassin auquel est rattaché votre établissement, rapprochez-vous de votre référent Éléa.

Vous pouvez identifier les bassins de notre académie sur ce site :

[http://www.ac-versailles.fr/pid35039/les-territoires-academie.html](http://www.ac-versailles.fr/pid35039/les-territoires-academie.html)

**Exemple** : pour le Val d'Oise, [http://cache.media.education.gouv.fr/file/contactsChezVous/34/7/Repertoire_etablissements_95_1045347.pdf](http://cache.media.education.gouv.fr/file/contactsChezVous/34/7/Repertoire_etablissements_95_1045347.pdf)

Voici la liste des **24 bassins d'éducation** de l'académie :

| Nom           | Adresse                                                      |
| ------------- | ------------------------------------------------------------ |
| Antony | [https://antony.elea.ac-versailles.fr](https://antony.elea.ac-versailles.fr) |
| Argenteuil | [https://argenteuil.elea.ac-versailles.fr](https://argenteuil.elea.ac-versailles.fr) |
| Boulogne | [https://boulogne.elea.ac-versailles.fr](https://boulogne.elea.ac-versailles.fr) |
| Cergy | [https://cergy.elea.ac-versailles.fr](https://cergy.elea.ac-versailles.fr) |
| Enghien | [https://enghien.elea.ac-versailles.fr](https://enghien.elea.ac-versailles.fr) |
| Etampes | [https://etampes.elea.ac-versailles.fr](https://etampes.elea.ac-versailles.fr) |
| Évry | [https://evry.elea.ac-versailles.fr](https://evry.elea.ac-versailles.fr) |
| Gennevilliers | [https://gennevilliers.elea.ac-versailles.fr](https://gennevilliers.elea.ac-versailles.fr) |
| Gonesse | [https://gonesse.elea.ac-versailles.fr](https://gonesse.elea.ac-versailles.fr) |
| Mantes | [https://mantes.elea.ac-versailles.fr](https://mantes.elea.ac-versailles.fr) |
| Massy | [https://massy.elea.ac-versailles.fr](https://massy.elea.ac-versailles.fr) |
| Montgeron | [https://montgeron.elea.ac-versailles.fr](https://montgeron.elea.ac-versailles.fr) |
| Mureaux | [https://mureaux.elea.ac-versailles.fr](https://mureaux.elea.ac-versailles.fr) |
| Nanterre | [https://nanterre.elea.ac-versailles.fr](https://nanterre.elea.ac-versailles.fr) |
| Neuilly | [https://neuilly.elea.ac-versailles.fr](https://neuilly.elea.ac-versailles.fr) |
| Poissy | [https://poissy.elea.ac-versailles.fr](https://poissy.elea.ac-versailles.fr) |
| Pontoise | [https://pontoise.elea.ac-versailles.fr](https://pontoise.elea.ac-versailles.fr) |
| Rambouillet | [https://rambouillet.elea.ac-versailles.fr](https://rambouillet.elea.ac-versailles.fr) |
| Sarcelles | [https://sarcelles.elea.ac-versailles.fr](https://sarcelles.elea.ac-versailles.fr) |
| Savigny | [https://savigny.elea.ac-versailles.fr](https://savigny.elea.ac-versailles.fr) |
| Saint-Germain | [https://sgl.elea.ac-versailles.fr](https://sgl.elea.ac-versailles.fr) |
| Saint-Quentin | [https://sqy.elea.ac-versailles.fr](https://sqy.elea.ac-versailles.fr) |
| Vanves | [https://vanves.elea.ac-versailles.fr](https://vanves.elea.ac-versailles.fr) |
| Versailles | [https://versailles.elea.ac-versailles.fr](https://versailles.elea.ac-versailles.fr) |


Après vous être connecté à Éléa pour repérer le **bassin d'éducation** de votre établissement, **déconnectez-vous**.

À présent la procédure va se dérouler en **deux étapes** à suivre **dans cet ordre** :

1. S'identifier une première fois sur la plateforme Éléa grâce à vos **identifiants académiques** ; ainsi, vous serez désormais aussi reconnu sous votre identité académique comme étant un utilisateur d'Éléa (et non plus seulement sous votre identité liée à votre ENT).
2. Accéder à nouveau via votre ENT à vos parcours Éléa existants, pour y créer ce second accès, par identification académique.

## Étape 1 – Se connecter à la plateforme Éléa de son établissement avec ses identifiants académiques

Si vous ne vous êtes jamais connecté à Éléa avec vos identifiants académiques, cette **première connexion est indispensable** pour avoir ultérieurement accès à vos parcours créés après une connexion via votre ENT (étape 2).

1.  Rendez-vous donc directement à l'adresse URL de votre plateforme Éléa : dans la barre d'adresse de votre navigateur internet entrez l'url correspondant à votre plateforme de bassin, sur le modèle **https://*BASSIN*.elea.ac-versailles.fr**.

![Se connecter à Éléa avec ses identifiants académiques](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture05connexionidentifiantsacademiques.png)

2. Une fois sur la page d'accueil, cliquez sur **Démarrer**.

3. Puis sur "**Utiliser mon compte académique**".
4. Connectez-vous avec vos identifiants académiques.

**Remarque** : après cette connexion l'affichage de votre tableau de bord professeur sera différent de celui qui s'affiche après votre connexion via votre ENT.

Ceci est normal : puisque deux jeux différents d'identifiants sont utilisés (identifiants ENT d'une part, et académiques d'autre part), du point de vue de la plateforme Éléa tout se passe comme s'il s'agissait de deux usagers distincts, et non pas d'une seule et même personne. 

![Deux affichages distincts pour deux connexions différentes](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture06deuxaccesdistincts.png)

Vous êtes désormais reconnu sur votre plateforme de bassin comme un usager d'Éléa avec vos identifiants académiques. 

**Déconnectez-vous** avant de passer à l'étape 2.

## Étape 2 – Créer dans ses parcours Éléa un accès utilisable avec ses identifiants académiques

1. Connectez-vous à nouveau votre plateforme Éléa, mais cette fois via votre ENT.
2. Dans votre tableau de bord professeur, pour chaque parcours pour lequel vous souhaitez créer un accès utilisable avec vos identifiants académiques, cliquez sur l'icone "**Liste des utilisateurs"**.

![Accéder à la liste des utilisateurs d'un parcours](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture07listedesutilisateurs.png)

3. Cliquez sur "**Inscrire des utilisateurs**".

![Inscrire de nouveaux utilisateurs](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture08inscriredenouveauxutilisateurs.png)

Vous allez créer votre accès avec vos identifiants académiques dans la fenêtre surgissante suivante.

![Fenêtre d'inscription](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture09fenetreinscription.png)

1. Entrez votre nom de famille : repérez alors dans la liste déroulante qui apparaît votre identifiant associé à votre **adresse mail académique**.

2. Sélectionnez le rôle "**Enseignant**". Ceci vous assure que, quand vous vous connecterez ultérieurement à ce parcours avec vos identifiants académiques, vous disposerez bien des **droits d'édition** sur ce parcours.

**Remarque** : si par erreur, vous omettez cette étape, il vous sera possible de rectifier ultérieurement le rôle associé à votre compte académique en cliquant dans la liste des utilisateurs sur "**Attribution des rôles**"![SeconnecteraEleaencasdefermeture10boutattributionrole](../img/images_Se_connecter_a_Elea_en_cas_de_fermeture_ENT/SeconnecteraEleaencasdefermeture10boutattributionrole.png), puis en sélectionnant "**Enseignant**".

3. Cliquez sur "**Inscrire les utilisateurs et cohortes sélectionnés**".

Désormais lorsque vous vous connecterez à la plateforme Éléa de votre bassin d'éducation par l'url **https://*BASSIN*.elea.ac-versailles.fr** en employant vos identifiants académiques, vous verrez apparaître ce parcours, et disposerez des droits d'édition sur celui-ci.

Répétez cette étape 2 pour chacun de vos parcours, pour vous assurer, en cas indisponibilité temporaire de votre ENT, un accès à ceux-ci avec vos identifiants académiques.

