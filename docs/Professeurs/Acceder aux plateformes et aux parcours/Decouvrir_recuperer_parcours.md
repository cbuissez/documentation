# Découvrir et récupérer des parcours de la Éléathèque

Découvrir un parcours

<iframe title="e-éducation &amp; Éléa - Découvrir des parcours de la Éléathèque" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/439a926b-0dae-48c8-86ee-52136ffe8b07" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Récupérer un parcours

<iframe title="e-éducation &amp; Éléa - Récupérer des parcours de la Éléathèque" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/5c321c0e-6074-4539-a468-49f3e83074e2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Étape 1 : Accéder à la Éléathèque

Pour accéder à la Éléathèque, il est nécessaire de se connecter à la plateforme et de s'identifier.

![page d'accueil élea](../img/eleatheque/page_accueil.png)    ![page d'accueil élea](../img/eleatheque/connexion.png)

Sur votre tableau de bord, cliquez sur "accéder à la Éléathèque".

![tableau de bord](../img/eleatheque/acceder_eleatheque.png)


## Étape 2 : Affiner sa recherche par mots-clés

Pour affiner votre recherche par niveau, discipline, approche pédagogique ou thématique.
Vous pouvez :
* sélectionner un des mots-clés les plus fréquemment utilisés dans la liste à droite de l'écran :
![mots-clés](../img/eleatheque/parcourir_eleatheque.png)
* taper votre recherche dans le champ texte (des propositions s'affichent en-dessous selon les caractères tapés) :
![recherche libre](../img/eleatheque/parcourir_eleatheque2.png)

Puis cliquez sur "Valider"

Une sélection de parcours correspond à votre recherche s'affiche comprenant le titre du parcours et l'ensemble des mots-clés associés.

![sélection de parcours](../img/eleatheque/selection_parcours.png)

## Étape 3 : Découvrir le contenu d'un parcours

Cliquez sur le titre du parcours pour obtenir des informations complémentaires :
* sur la partie droite s'affiche une description du parcours et le nom des auteurs.
* sur la partie gauche s'affiche le scénario du parcours avec l'ensemble des activités.

![exemple de présentation de parcours](../img/eleatheque/exemple_parcours.png)

En cliquant sur la première activité du parcours ou sur "tester le parcours", vous accédez à la plateforme communaute.elea où sont stockés les parcours exemples académiques.
Sur cette plateforme, vous devez vous identifier avec votre compte académique.
Puis, vous pouvez consulter directement le parcours que vous avez choisi avec un statut d'enseignant non-éditeur.
Vous avez accès à toutes les parties y compris celles cachées aux élèves et vous n'êtes pas bloqué dans votre progression. Par contre, vous ne pouvez pas modifier le parcours.

![tester le parcours](../img/eleatheque/tester_parcours.png)

Pour tester le parcours comme les élèves, cliquez sur le menu bleu puis sur "Prendre le rôle de..." et enfin choisissez "ÉTUDIANT" 

En cliquant sur une activité, vous accédez à la page correspondante sur le parcours sans avoir à réaliser la progression demandée aux élèves.


## Étape 4 : Télécharger un parcours

Si vous souhaitez récupérer un parcours pour le proposer à vos élèves tel quel ou le personnaliser, vous pourrez le télécharger depuis la Éléathèque.
Cependant, la fermeture prochaine de la plateforme « ScolawebTV » au profit du nouveau service d’hébergement vidéo « Peertube » implique pour les utilisateurs d’éléa de migrer toutes ces anciennes vidéos sur le nouveau service et de rééditer les codes d’intégration dans leurs parcours.
Tous les parcours de la Éléathèque contenant des vidéos de la ScolawebTV ont été actualisés et intègrent désormais des vidéos hébergées sur Peertube. Si vous testez un parcours, vous aurez bien accès au parcours mis à jour.
En revanche, l’archive que vous récupérez en cliquant sur le bouton « Télécharger le parcours » est obsolète et les vidéos qu’elles contiennent sont encore hébergées sur l’ancienne plateforme (il est à noter que si le parcours ne contient aucune vidéo, vous pourrez le récupérer en utilisant ce bouton et passer à la dernière étape).

Pour récupérer le parcours actualisé, veuillez suivre la procédure suivante :

Cliquez sur « Tester le parcours ».

![télécharger un parcours](../img/eleatheque/accueil.png)

Allez dans le menu « Administration du cours ».

![menu1](../img/eleatheque/menu1.png)

Cliquez sur le bouton « Sauvegarde ».

![menu2](../img/eleatheque/menu2.png)

En bas de la page, cliquez sur « Passer à la dernière étape ».

![sauvegarde1](../img/eleatheque/sauvegarde1.png)

Cliquez sur « continuer ».

![sauvegarde2](../img/eleatheque/sauvegarde2.png)

Un message d’erreur apparaît. C’est tout à fait normal étant donné que vous ne possédez pas les droits pour restaurer ce parcours sur la Éléathèque.

![sauvegarde3](../img/eleatheque/sauvegarde3.png)

En revanche, la sauvegarde du parcours est bien enregistrée dans votre compte académique.
Pour récupérer l’archive, cliquer sur « continuer », puis revenez sur votre page d’accueil à partir du fil d’ariane.

![lien-accueil](../img/eleatheque/lien-accueil.png)

Vous pouvez maintenant restaurer le parcours sur la plateforme communauté, où récupérer l’archive afin de la restaurer sur votre plateforme de bassin.
Créez un nouveau parcours vide et donnez lui un nom.

![parcours-vide](../img/eleatheque/parcours_vide.png)

Puis dans le menu « administration du cours », cliquez sur « restauration ».

![restauration](../img/eleatheque/restauration.png)

Vous accédez au gestionnaire des sauvegardes. 

![restauration2](../img/eleatheque/restauration2.png)

Vous pouvez désormais, 
* soit restaurer le parcours sur la plateforme communauté pour travailler avec des collègues de l’académie en cliquant sur « restaurer » (non utilisable avec les élèves)

* soit récupérer l’archive du parcours (fichier .mbz) pour le restaurer sur votre plateforme de bassin afin de le mettre à disposition de vos élèves.
Une fois le parcours téléchargé, suivez la procédure de [migration d'un parcours](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Acceder%20aux%20plateformes%20et%20aux%20parcours/Faire_migrer_un_parcours/ "tutoriel pour migrer un parcours") à partir de l'étape de 2. Sur ce nouveau parcours, identique au parcours exemple académique, vous aurez des droits d'enseignant éditeur et aurez la possibilité de le modifier.





