## Créer la première paire d'images

![Interface de l'activité Flashcards Cartes flash](../img/imagespairingappariementimages/imagespairingappariementimages02interface.png)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez ici la consigne de l'activité. 

**Exemple** : "Déplacez chaque animal sur ses empreintes."

3. **Image** : Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier souhaité d'une première image.
**Remarque** : pour offrir le meilleur affichage possible à vos élèves prévoir des images de format **carré** (idéalement redimensionnées au format 105 pixels par 105 pixels).
5. Rédigez ici un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.
    ![Interface de création de la première image d'une paire](../img/imagespairingappariementimages/imagespairingappariementimages03interface.png)
    5.et 6. **Image Correspondante** : recommencez les deux dernières étapes pour l'image qui devra être associée à la précédente.

![Interface de création de la deuxième image d'une paire](../img/imagespairingappariementimages/imagespairingappariementimages04interface.png)

Pour supprimer une image (en vue de lui en substituer une autre par exemple), cliquez sur la croix **x** en haut à droite de celle-ci.

![Icône de suppression d'image](../img/imagespairingappariementimages/imagespairingappariementimages05suppresseonimage.png)

Votre première paire d'images à apparier est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/imagespairingappariementimages/imagespairingappariementimages06boutonenregistreretafficher.png)

## Créer et organiser les paires d'images suivantes

![Interface d'ajout et de navigation des paires d'images](../img/imagespairingappariementimages/imagespairingappariementimages07ajoutetnavigationentrelespaires.png)

1. Cliquez sur "**+ AJOUTER CARTE**" autant de fois que nécessaire pour ajouter à la série autant de paires d'images supplémentaires que souhaité. Pour chaque nouvelle paire on recommencera les quatre opérations précédentes (ajout des deux images à associer entre elles et rédaction du texte alternatif de substitution pour chacune).
2. Pour naviguer entre les différentes paires d'images, cliquez sur le titre de la paire à modifier dans la barre grisée à gauche. Vous pouvez aussi modifier  dans l'activité l'ordre des paires entre elles, en cliquant sur les flèches ▲ et ▼ pour monter ou descendre une paire d'images par rapport aux autres. Cliquez sur la croix **x** pour supprimer définitivement une paire d'images.

## Éditer les images (optionnel)

Sous chaque image ajoutée un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/imagespairingappariementimages/imagespairingappariementimages08boutonediterimage.png)


![Menu d'édition d'image](../img/imagespairingappariementimages/imagespairingappariementimages09menueditionimages.png)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/imagespairingappariementimages/imagespairingappariementimages10enregistrerimage.png)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'album pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](../img/imagespairingappariementimages/imagespairingappariementimages11boutoncopyright.png)

## Bouton recommencer et options

![Bouton recommencer](../img/imagespairingappariementimages/imagespairingappariementimages12comportementsetoptions.png)

1. En cochant cette option, l'élève disposera d'un bouton "**Recommencer**" pour relancer une nouvelle tentative au terme de l'activité.

2. Enfin le menu "**Options et Textes**" permet  de modifier les libellés des textes et boutons de l'activité. Les mentions @score ou @total sont des mentions génériques auxquelles se substitueront respectivement, le score effectivement obtenu par l'élève pour une tentative donnée, et le nombre de points qui pouvaient être obtenus au total dans l'activité créée.