# Essay (Rédaction guidée)

Ce contenu interactif (H5P) demande à l'élève de rédiger un court texte répondant à la consigne du professeur. 

Cette activité est autocorrective et propose une auto-évaluation formative à l'élève. Le professeur ne récupèrera pas le texte rédigé ici par l'élève mais seulement une note calculée selon les occurrences de mots clefs prédéfinis. Le contenu interactif **Rédaction guidée** est donc un outil d'aide à la rédaction qui accompagne les élèves en amont de la production de leur rédaction définitive sur un document papier, ou bien dans une question composition d'une activité Test, ou bien encore d'une rédaction de texte en ligne dans une activité Devoir (car dans ces deux derniers cas le professeur récupérera à fin d'évaluation le texte final saisi par élève).

Nous commencerons par créer un contenu interactif de type (en anglais) "**Essay**".

![Icone de l'activité Fill in the Blanks Texte à Trous](../img/imagesessayessai/essayessai00icone.png)
