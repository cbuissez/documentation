# Image Sequencing (img par ordre logique)

Avec ce contenu interactif (H5P) l'élève se voit proposer une série d'images sur un même thème : il s'agira alors par glisser-déposer à la souris d'ordonner ces images entre elles selon l'ordre qui est spécifié dans la consigne.

Ce contenu interactif est donc adapté à l'étude de toute notion qui inclut une dimension chronologique : par exemple remettre en ordre des illustrations en fonction d'un récit déjà connu de l'élève en Lettres ou en Histoire ; ordonner les étapes à respecter d'un protocole d'expérience en Sciences ; ou bien encore classer des œuvres pour une période donnée en Histoire de l'Art. 

![Exemple d'une activité de séquence d'images : cycle de vie d'une plante](../img/imagesequencingsequenceimages/imagesequencingsequenceimages01exempleactivité.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Sequencing**".

![Icone de l'activité image sequencing Images par ordre logique](../img/imagesequencingsequenceimages/imagesequencingsequenceimagesicone.png)