# Flashcards (Cartes de révision)

Voici un contenu interactif qui vous permettra de créer l'équivalent numérique de cet outil classique de révision que sont des **Cartes de révision** : une série de cartes à faire défiler rapidement, à raison d'une question à réponse courte par carte. 

![Exemple de Flashcards Cartes de révision](../img/imagesflashcardscartesflash/flashcardscartesflash01exemple.png)

L'association d'une image (qui peut elle-même d'ailleurs contenir des éléments textuels, des schémas ou formules) favorise encore davantage l'apprentissage en offrant un **double encodage** des informations à retenir : un encodage textuel sous forme de Question/Réponse et, en plus, un encodage visuel.

Ce questionnement -"en rafale"- avec rétroactions immédiates offre donc aux élèves une auto-évaluation formative des plus efficaces.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Flashcards**".

![Icône de l'activité Flascards Cartes de révision](../img/imagesflashcardscartesflash/flashcardscartesflashicone.png)