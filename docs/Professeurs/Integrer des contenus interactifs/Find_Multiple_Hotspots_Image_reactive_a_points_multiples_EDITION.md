## Créer l'arrière-plan de l'activité

![Interface de l'activité Find Mutliple Hotspots Image réactive à points multiples](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer03interface.png)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.
2. Indiquez ici un sous-titre. Ce sous-titre peut permettre de souligner une nuance par rapport au titre précédent, c'est-à-dire une caractéristique qui permettra de discriminer les bonnes des mauvaises réponses dans l'image proposée. 

3. **Image d'arrière-plan** : cet onglet permet de déposer l'image d'arrière-plan de l'activité. 

4. Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. C'est sur cette image que reposera toute l'activité en fournissant en arrière-plan à l'élève les différents éléments à distinguer entre eux et sur lesquels il s'agira de cliquer.

L'image ajoutée à l'activité peut être supprimée (afin par exemple de lui en substituer une autre) avec l'icone croix **x** en haut à droite.

![Icône de suppression d'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer04interface.png)

Sous l'image ajoutée à l'activité un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer05boutonediterimage.png)


![Menu d'édition d'image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer06menueditionimages.png)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer07enregistrerimage.png)

**Remarque** : un bouton copyright permet de renseigner les crédits et droits d'usage de l'image. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer08boutoncopyright.png)

5. Cliquez enfin sur  "**Hotspots**" (zones réactives) pour passer à l'onglet suivant et poursuivre la création de l'activité. 

## Créer les "zones réactives"

![Interface de création des Hotspots ou "zones réactives"](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer09interfacehotspotszonesreactives.png)



1. Rédigez dans ce champ la consigne.

**Exemple** : Cliquez sur les drapeaux des pays qui ne sont pas membres de l'Union Européenne.

2. Indiquez ici le type d'éléments qui est à rechercher dans l'image : cette mention sera employée en complément dans les rétroactions qui suivront chaque clic de l'élève.

**Exemple** : la mention "**des pays hors de l'U.E. cachés ici.**" de l'exemple apparaitra dans des rétroactions automatisées libellées ainsi : "Vous avez trouvé 1 sur 3 **des pays hors de l'U.E. cachés ici**".

3. Renseignez dans ce champ le **nombre minimum** de clics corrects (c'est-à-dire de clics sur des éléments correspondants à la consigne) qui sont attendus pour que l'activité soit considérée comme ayant été réussie par l'élève.

4. **Hotspots** : cliquez sur la forme souhaitée disque ou carré pour la première "zone réactive" à dessiner.

![Fenêtre surgissante des Hotspots ou zones réactives](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer10fenetresurgissantedeshotspotszonesreactives.png)

Dans la fenêtre surgissante qui apparaît :

1. Si la "zone réactive" sur le point d'être tracée est bien une zone sur laquelle l'élève devra cliquer dans l'activité cochez "**Correct**", sinon laissez décoché.

2. Il est possible de rédiger ensuite une rétroaction spécifique qui surgira après un clic sur cette "zone réactive".

3. Cliquez enfin pour créer la "zone réactive" (ou annulez en cliquant sur la mention en rouge).

## Positionner et dimensionner les "zones réactives"

Les éléments "zones réactives" peuvent être déplacées au-dessus de l'image d'arrière-plan par cliquer-glisser, et peuvent être redimensionnées en tirant sur les "poignées" carrées blanches du pourtour de la zone sélectionnée.

![Positionner et redimensionner les zones réactives](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer11zonesreactives.png)

**Remarque**: en maintenant la **touche majuscule** du clavier enfoncée et en jouant sur la  poignée inférieure droite de la "zone réactive" choisie, celle-ci sera redimensionnée de manière proportionnelle en longueur et largeur à ses dimensions d'origine. Il est aussi possible d'utiliser les quatre flèches du clavier pour déplacer précisément la "zone réactive" sélectionnée.

Pour rappel ces "zones réactives " resteront totalement invisibles pour l'élève.

Il s'agit donc de placer chaque "zone réactive" transparente en cohérence visuellement avec un des éléments distinctifs de l'image d'arrière-plan choisie (élément correspondant ou non à la consigne selon que la zone a été précédemment déclarée correcte ou non).

Pour un positionnement ou un redimensionnement plus précis on utilisera les outils du menu flottant de la "zone réactive" sélectionnée.

![Menu flottant des "zones réactives"](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer12menuflottantdeszonesreactives.png)

Il est enfin possible d'améliorer la guidance de l'activité avec deux dernières rétroactions.

![Améliorer la guidance de l'activité](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer13guidance.png)

1. Ce message s'affichera si l'élève clique sur l'image en dehors de toute "zone réactive".
2. Ce message s'affichera si l'élève clique à nouveau sur une "zone réactive" où il avait précédemment déjà cliqué.

L'activité"**Image à cliquer**" est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer14boutonenregistreretafficher.png)