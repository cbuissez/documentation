# Find the Words (Mots Mêlés)

Ce contenu interactif (H5P) permet de créer une grille de jeu du type "Mots Mêlés" : il s'agira pour l'élève de repérer puis d'entourer à la souris tous les mots qui ont été dissimulés dans une grille de lettres apparemment désordonnées qui s'affiche à l'écran. 

![Une grille de "Mots Mêlés"](../img/findthewordsmotsmeles/findthewordsmotsmeles01grillevierge.png)

Ces mots auront été préalablement choisis par l'enseignant et se retrouveront écrits aussi bien à l'envers qu'à l'endroit, horizontalement, verticalement ou même en diagonale. 

À chaque nouvelle tentative les mots seront cachés différemment.

![Cette grille de "Mots Mêlés" en cours de résolution](../img/findthewordsmotsmeles/findthewordsmotsmeles02grilleenjeu.png)

Nous commencerons par ajouter un contenu interactif du type (en anglais) "**Find the Words**".

![Icône de l'activité Find the Words Mots mêlés](../img/findthewordsmotsmeles/findthewordsmotsmelesicone.png)