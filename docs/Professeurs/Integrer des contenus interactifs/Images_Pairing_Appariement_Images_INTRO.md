# Image Pairing (Appariement d'images)

Ce contenu interactif (H5P) propose une activité d'appariement, les deux éléments de chaque paire à retrouver étant chacun une image. Les deux séries d'images se présenteront en deux colonnes : l'élève devra cliquer-déplacer les images de la colonne de gauche sur l'image correspondante dans la colonne de droite.

![Exemple d'appariemment d'images](../img/imagespairingappariementimages/imagespairingappariementimages00exemple.png)

L'activité pourra aussi permettre d'associer par exemple des illustrations (schémas, photographies, tableaux, etc.) et leurs légendes (c'est-à-dire des textes mais enregistrés ici sous forme d'images).

![Exemple d'appariemment d'images corrigé](../img/imagespairingappariementimages/imagespairingappariementimages01exemple.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Pairing**".

![Icône de l'activité Image Pairing Appariement d'Images](../img/imagespairingappariementimages/imagespairingappariementimagesicone.png)