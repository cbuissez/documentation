# Accordion (Menu déroulant)

Le contenu interactif **Menu déroulant** permet d’intégrer un texte sous forme d’une série de "blocs" de texte emboîtés et déroulables les uns sous les autres par un simple clic sur le bloc souhaité. 

Le texte est ainsi structuré en différentes sections empilées verticalement, sections que l’utilisateur dépliera à la manière d’un "Menu déroulant" pour accéder progressivement aux différents contenus (à raison d'une seule section affiché à la fois). 

**Exemple** : la deuxième section du **Menu déroulant** est ici dépliée.

![Exemple d'accordéon](../img/accordionaccordeon/accordionaccordeon01exemple.png)

L’activité **Menu déroulant** est donc un bon moyen d’offrir une présentation interactive pour des textes. 

Et cette présentation est particulièrement adaptée pour des textes longs car ils seront ainsi d’un accès plus attractif en étant organisés en sections cliquables une à une.

**Exemple** : la troisième section du **Menu déroulant** est ici dépliée.

![accordionaccordeon02exemple](../img/accordionaccordeon/accordionaccordeon02exemple.png)

**Remarque** : Seul du texte peut être intégré comme contenu dans un tel **Menu déroulant** (mais pas d’images ou de vidéos). Ce texte peut toutefois contenir des liens internet cliquables.

Nous commencerons par créer un contenu interactif du type (en anglais) "**Accordion**".

![Icone de l'activité accordéon](../img/accordionaccordeon/accordionaccordeonicone.png)
