## Créer la première section du Menu déroulant

![Interface d'édition de l'activité Menu déroulant](../img/accordionaccordeon/accordionaccordeon03interface.png)

1. Donnez un titre au **Menu déroulant** : c'est sous ce titre que l'activité apparaîtra dans le parcours Éléa.
2. Renseignez le "**Titre**" de la première section de texte qui apparaitra en haut du **Menu déroulant**.
3. "**Texte**" : saisissez ici le contenu de cette première section. Vous disposez dans la barre d’outils des outils usuels d'un éditeur de texte : mise en gras ou en italique, disposition centrée, à droite ou à gauche des paragraphes, insertion/suppression de liens hypertextes ou de lignes horizontales de séparation, choix des styles et tailles de polices de caractères, ainsi que des couleurs d'écriture ou de fond.
4. **Remarque** : cliquez-tirez sur la flèche en bas à droite de l'éditeur de texte permet de redimensionner celui-ci pour un meilleur confort de travail.

## Ajouter et organiser les sections du Menu déroulant

![Icônes de navigation dans le menu déroulant](../img/accordionaccordeon/accordionaccordeon04interface.png)

Le texte du **Menu déroulant** va donc être organisé en différentes sections (appelées ici "**panels**"). 

1. Cliquez que bouton "**AJOUTER PANEL**" pour créer la section suivante du **Menu déroulant**.

![Le bouton ajouter "panel"](../img/accordionaccordeon/accordionaccordeon05boutonajouterpanel.png)

Chaque section comportera un titre distinctif : l’utilisateur cliquera sur ce titre de section pour la faire apparaître en déroulé et pouvoir ainsi la lire (la précédente section qui avait été "ouverte" se repliera alors automatiquement de son côté).

2. Lors de la création du **Menu déroulant**, pour votre confort de lecture à l'écran, il vous est possible de condenser les informations (titre et contenu) d'une section donnée en cliquant sur la flèche blanche **▼** qui est située dans la barre bleue de titre. Ultérieurement, cliquez sur la flèche ► pour redéplier les informations d'une section afin de pouvoir à nouveau les modifier.
3. Les sections du **menu déroulant** peuvent être déplacées entre elles (montées dans la liste en cliquant à droite sur **▲**, ou descendues en cliquant sur **▼**) ce qui modifiera leur ordre d'empilement vertical dans le **Menu déroulant**. Cliquer sur la croix **x** permet de supprimer entièrement une section donnée.

Votre **Menu déroulant** est prêt : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour le parcourir.

![Le bouton enregistrer et afficher](../img/accordionaccordeon/accordionaccordeon06boutonenregistreretafficher.png)

(Crédits textes : WIKIPEDIA)
