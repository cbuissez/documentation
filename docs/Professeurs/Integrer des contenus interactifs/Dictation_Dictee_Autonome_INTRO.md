# Dictation (Dictée autonome)

À partir d'un simple fichier son, vous pourrez avec ce contenu interactif (H5P) **Dictée autonome** créer des dictées autonomes : l'élève écoute et saisit au clavier ce qu'il entend ; puis son texte est alors automatiquement corrigé.

Ce contenu interactif est à employer préférablement avec des textes courts. Il favorise l'apprentissage des règles de grammaire et de l'orthographe en Lettres et Langues Étrangères. Mais il pourra être aussi mis à profit dans toute autre discipline dès qu'intervient un apprentissage d'un vocabulaire spécifique à connaître, ou, par exemple, de propriétés et de définitions à réciter.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Dictation**".

![Icône de l'activité Dictation Dictée autonome](../img/imagesdictationdictee/dictationdicteeicone.png)
