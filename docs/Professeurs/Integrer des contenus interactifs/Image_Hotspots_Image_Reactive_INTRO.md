# Image Hotspots (Image réactive)

<iframe title="e-éducation &amp; Éléa - Intégrer une image interactive H5P" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/bf12c589-3f96-46c9-aa76-b446270ca508" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Ce contenu interactif permet de créer des images ou des infographies réactives : des "puces" sont réparties sur une image d'arrière plan ; un clic sur une de ces puces donne accès à des informations sous forme de textes, d'images ou de vidéos.

![Exemple d'image ou d'infographie](../img/hotspotsinfographiereactive/hotspotsinfographiereactive01exemple.png)



Il est ainsi possible d'enrichir n'importe quel document préexistant (enregistré sous forme d'une image) avec des points d'intérêt cliquables qui fournissent des informations sur certains détails représentés.

![Exemple de puce cliquée](../img/hotspotsinfographiereactive/hotspotsinfographiereactive02exemple.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Hotspots**".

![Icone Hotspots Image réactive](../img/hotspotsinfographiereactive/hotspotsinfographiereactiveicone.png)
