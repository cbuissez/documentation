# Intégrer une activité Base de données

## Tutoriel pour le professeur
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Base de données dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/1046e7c4-4d15-4be7-96e3-7dcd156947be" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Tutoriel pour les élèves
<iframe title="e-éducation &amp; Éléa - Tutoriel élève : la base de données" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/c5937864-a2b9-49d7-88a3-3bef00bfaaed" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
