# Convertir une activité QCM, Appariement, Millionnaire en Test

Les 3 activités QCM, jeu d'Appariement et jeu du millionnaire sont des activités **natives** d'Éléa, mais elles peuvent être facilement converties en activité Test afin de permettre la restauration d'un parcours sur n'importe quelle plateforme Moodle.

Voici la démarche à suivre pour effectuer cette conversion.

Sur le parcours contenant une de ces 3 activités, passez en mode édition. Cliquez sur l'activité que vous souhaitez convertir.

Vous pourrez alors vous rendre dans le menu "sandwitch" en cliquant sur l'icône. Dans le menu, vous disposez d'un menu Administration "de l'activité" (sur la capture ci-dessous, pour un jeu d'appariement).


![menu sandwitch](../img/convertir/Capture1.png) 


En entrant dans ce menu, vous aurez alors la possibilité de convertir cette activité en activité test.

![menu convertir](../img/convertir/Capture2.png) 

L'activité Test est alors ajoutée sous l'activité native Éléa et porte le même nom.

![activité dupliquée](../img/convertir/Capture3.png) 

