# Intégrer une activité Devoir

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Devoir dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/ed61717f-a25d-4d78-9fdd-326982d0c9aa" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Étape 1 : Ajouter une activité devoir

Pour créer un devoir, ouvrez le parcours sur lequel vous souhaitez ajouter une activité **Devoir**, cliquez en haut à droite sur le bouton "**Activer le mode édition**". Sélectionnez ensuite la section concernée puis :

- Cliquez sur **"Ajouter une activité ou une ressource"** ;
- Passez en **"Mode simple"** ;
- Sélectionnez l'activité **Devoir** dans la liste;
- Et enfin cliquez sur **"Ajouter"**.

Une nouvelle page s'ouvre pour définir les paramètres de l'activité.

## Étape 2 : Intégrer l'activité 

Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

Vous pouvez ajouter un fichier modèle ou un fichier complémentaire pour aider les élèves à rendre le devoir.

![Interface du dépôt de fichier](../img/activitedevoir/activitédevoir01depotfichier.png)

Vous pouvez choisir le type de remise : soit les élèves vont devoir saisir du texte en ligne, soit ils pourront déposer un fichier.

Il y a enfin des paramètres qui affinent la remise des différents travaux : travail individuel, travail de groupe.

Il est possible ensuite de paramétrer votre activité devoir (restrictions d'accès, achèvement d'activités...) : [Paramétrer des activités : suivi d'achèvement et restrictions d'accès ](?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces)
