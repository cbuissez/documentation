# Intégrer une activité Leçon

## La leçon thématique
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Leçon thématique dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/0b4720e3-eec9-491c-9f29-44449b6cd789" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## La leçon différenciée
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Leçon différenciée dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/c7134b8e-d977-4641-9fb4-a6c5b5dfd385" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
