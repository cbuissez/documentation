# Intégrer une activité Test

## Créer des questions et définir les paramètres généraux

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (1/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/f05bf2c1-5da5-43f4-bbb3-a75b8be3c289" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Paramétrer des feedbacks (en fonction des réponses)

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (2/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/3f25128f-4d85-4ae6-a1a5-137121678f2a" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Récupérer les résultats des élèves et corriger leurs réponses

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (3/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/e5b8a6ea-93b6-461a-a40e-222c40e36af2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
