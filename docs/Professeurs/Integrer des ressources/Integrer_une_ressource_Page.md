# Intégrer une ressource Page

<iframe title="e-éducation &amp; Éléa : Intégrer une ressource Page à un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/d8f528bb-35e3-4607-83c3-71f4c883b06d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Créer une ressource Page
Pour créer une activité "**Page**", ouvrez le parcours sur lequel vous souhaitez ajouter une activité page, sélectionnez la section concernée puis :

- Cliquez sur **"Ajouter une activité ou une ressource" **;
- Sélectionnez l'activité **"Page"** dans la liste ;
- Et enfin cliquez sur **"Ajouter"**.      

## Paramétrer votre ressource

Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description que vous pouvez aussi choisir d'afficher sur la page de cours en cochant la case en bas de la cellule description.

![Nom et description de la page](../img/ressourcepage/ressourcepage01description.png)

Complétez l'onglet contenu, en insérant le contenu que vous souhaitez voir figurer sur votre page.

![Contenu de la page](../img/ressourcepage/ressourcepage02contenu.png)


Il est enfin possible de paramétrer votre ressource page (restrictions d'accès, achèvement d'activités...) : [Paramétrer des activités : suivi d'achèvement et restrictions d'accès ](?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces)
