# Intégrer une ressource Fichier

<iframe title="e-éducation &amp; Éléa - Intégrer une ressource Fichier dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/ae2e1574-5e41-407c-9388-b56b01878571" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

**Étape 1 - Ajouter la ressource Fichier**

Pour créer une ressource **Fichier**, ouvrez le parcours sur lequel vous souhaitez l'ajouter, cliquez en haut à droite sur le bouton "Activer le mode édition". Sélectionnez ensuite la section concernée et :

- cliquez sur **"Ajouter une activité ou une ressource"**,

- passez en **"Mode Avancé"**,

- sélectionnez la ressource **Fichier** dans la liste, 

![choixdelaressourcefichier](../img/integrerressourcefichier/choixdelaressourcefichier.png) 

- et enfin cliquez sur **"Ajouter"**.

**Étape 2 - Choisir l'usage du Fichier**

- <u>Fichier non modifiable par les élèves</u>
- PDF, images, vidéos, son, ...

Saisissez le titre dans la case **"Nom"**. Ce titre doit être explicite et avoir du sens par rapport au parcours et aux élèves. Écrivez la consigne dans la partie **"Description"**

![nometdescription](../img/integrerressourcefichier/nometdescription.png)

Ajoutez le fichier dans le cadre **"Sélectionner des fichiers"**. Choisissez le document dans l'ordinateur et **"Déposer un fichier"** dans le parcours

![selectionnelefichier](../img/integrerressourcefichier/selectionnelefichier.png)

![telechargerle fichierdansleparcours](../img/integrerressourcefichier/telechargerle fichierdansleparcours.png)

Pour faciliter l'accès aux élèves, ouvrez par le menu **"Apparence"** et choisir l'affichage en sélectionnant **"Intégrer"**.

![integrer](../img/integrerressourcefichier/integrer.png)

Cliquez sur **"Enregistrer et afficher"** en bas de la page et vous verrez le fichier tel qu'il apparaîtra à l'écran dans le parcours.

- <u>Fichier à télécharger pour être modifiable par les élèves</u>
  - Textes, diaporamas, tableurs, ... 

Saisissez le titre dans la case **"Nom"**. Ce titre doit être explicite et avoir du sens par rapport au parcours et aux élèves. Écrivez la consigne dans la partie **"Description"**.

![nometdescription](../img/integrerressourcefichier/nometdescription.png)

Ajoutez le fichier dans le cadre **"Sélectionner des fichiers"**. Choisissez le document dans l'ordinateur et **"Déposer un fichier"** dans le parcours.

![selectionnelefichier](../img/integrerressourcefichier/selectionnelefichier.png)

![telechargerle fichierdansleparcours](../img/integrerressourcefichier/telechargerle fichierdansleparcours.png)Pour faciliter l'accès aux élèves, ouvrez par le menu **"Apparence"** et choisir l'affichage en sélectionnant **"Ouvrir dans une fenêtre surgissante"**.

![dansunefenetresurgissante](../img/integrerressourcefichier/dansunefenetresurgissante.png)

Cliquez sur **"Enregistrer et afficher"** en bas de la page et vous verrez le lien qui permettra aux élèves de télécharger le fichier.
