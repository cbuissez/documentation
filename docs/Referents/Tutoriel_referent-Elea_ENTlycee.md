# Créer les comptes utilisateurs avec l'ENT MonLycée.net

## Etape 1 - Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à votre ENT https://ent.iledefrance.fr avec votre compte, puis dans la liste des applications, choisissez **Administration** (uniquement accessible aux administrateurs de l’ENT).

![administration](img/ent91/capture1.png) puis ![administration](img/ent91/admin-icon.png)

La console d'administration apparaît à l'écran :

![exports](img/entidf/console.png)

Cliquez sur **Exporter des comptes** dans la section **Imports / Exports**.

Dans le nouvel écran qui s'affiche :

![export](img/entidf/export.png)

1. Rendez-vous dans l'onglet **Exporter des comptes** ;
2. Vérifiez que les valeurs reprennent bien celles de la capture ci-dessus ;
3. Cliquez sur **Exporter**.

Il ne vous reste plus qu’à enregistrer le fichier **« export.csv »** à l’endroit que vous souhaitez.

![docs_à_télécharger](img/ent91/capture4.png)

**Remarque** : Le fichier doit impérativement se nommer  **« export.csv »** et vous devrez le détruire à la fin de la procédure. Si le fichier se nomme autrement (par exemple **« export (1).csv »**), c'est que vous avez sans doute omis de supprimer le précédent export réalisé sur votre machine. Supprimez alors le plus vieux et renommez le plus récent.

## Etape 2 - Se connecter à la plateforme Elea de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant **d'activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs.**

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

![accueil_elea](img/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local ».**

![accueil_elea](img/accueil_elea/capture2.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](img/accueil_elea/capture3.png)

## Etape 3 - Importer vos comptes utilisateurs dans Elea

Sur la page d'accueil, cliquez sur **« NOUVELLE IMPORTATION »**.

![import](img/import/capture9_idf.jpeg)

Puis cliquez sur **« Choisir un fichier »** ou **glissez/déposez** celui-ci.

![import](img/import/capture9.png)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](img/import/capture7.png)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](img/import/capture10.png)
