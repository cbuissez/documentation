# Créer les comptes utilisateurs avec l'ENT Oze

<iframe title="e-éducation &amp; Éléa - Intégrer des questions calculées dans une activité Test" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/6215a2d0-f6b2-4f3e-8e36-3cf35d976b13" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Étape 1 - Exporter vos comptes utilisateurs depuis l'ENT

**Connectez-vous à votre ENT** avec un compte administrateur puis cliquez sur le menu bleu en haut à droite.

![Mes Oz'apps](img/ent_oze/menu.png)

Dans **« Mes Oz'Apps favorites »**, cliquez sur **« Annuaire »**.

![Mes Oz'apps](img/ent_oze/OzApps.png)

Cliquez sur **« AFFICHER TOUS LES UTILISATEURS »**.

![Profil](img/ent_oze/Annuaire_profil.png)

Puis sur **« Exporter »** et enfin sélectionnez **« SACoche(CSV) »**.

![Exporter](img/ent_oze/Export_SACoche.png)

Enregistrez le fichier.

![Enregistrer](img/ent_oze/Enregistrer_sous.png)

**Remarque** : Pour des questions juridiques, tous les fichiers exportés doivent être **détruits** dès la fin de la procédure.  


## Étape 2 - Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant d'activer le compte local de gestion de l'établissement sur la plateforme Éléa, puis d'y importer les comptes utilisateurs.

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

![accueil_elea](img/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local ».**

![accueil_elea](img/accueil_elea/capture2_78.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](img/accueil_elea/capture3.png)

## Étape 3 - Importer vos comptes utilisateurs dans Éléa

Cliquez sur **« NOUVELLE IMPORTATION »**.

![import](img/ent_oze/nouvelle_importation.png)

Cliquez sur **« Choisir un fichier »** ou **glissez/déposez** le fichier que vous avez enregistré à l'étape précédente.

![import](img/import/capture9.png)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](img/ent_oze/import_etape2.png)

![import](img/ent_oze/import_etape3.png)

![importation_terminée](img/ent_oze/import_etape4.png)

