# Créer les comptes utilisateurs avec SIECLE 2nd degré

Pour pouvoir utiliser Éléa sans une identification via un ENT, il est nécessaire de déposer auparavant une **demande d’avis à la CNIL.**
Voir la documentation complémentaire liée à ce tutoriel.

## Étape 1 – Récupérer le fichier généré à partir de SIECLE

Demandez à votre direction de vous fournir le fichier **« élèves sans adresse »** exporté depuis **l’application SIECLE**. Il vous permettra de procéder à la création des comptes utilisateurs des élèves sur la plateforme Éléa de votre établissement.

À partir du portail Arena, choisissez **« Scolarité du 2nd degré »** dans le menu puis **« Consultation et export »**.
Vérifiez l’année sélectionnée puis cliquez sur **« Entrer »**.

![portail_ARENA](img/import/capture1.png)

Dans le **menu « Exportations »**, choisissez **« En XML »** puis **« Elèves sans adresse »**.

![exportations_xml](img/import/capture2.png)

Vous obtenez un **fichier compressé** qui peut-être utiliser tel quel sur la plateforme Éléa.

![enregistrer_dossierzip](img/import/capture3.png)

## Étape 2 – Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant d'**activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs**.

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

![page_accueil](img/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local »**, puis entrez **l'UAI (RNE) et le mot de passe**.

![connexion](img/accueil_elea/capture2.png)

## Étape 3 – Importer vos comptes utilisateurs dans Éléa

Une fois connecté, cliquez sur **« Choisir un fichier »** ou **glissez/déposez** celui-ci.
![import](img/import/capture6.png)
Choisissez entre les deux options proposées en fonction de votre situation.

**Attention !** Pour les établissements ayant déjà accès à la plateforme Éléa, les deux options qui vous sont proposées sont importantes. Vous pouvez :
- conserver les mots de passe créés précédemment en cliquant sur le bouton vert ;
- générer de nouveaux mots de passe pour tous les utilisateurs en cliquant sur le bouton orange (option obligatoire si vous n’avez pas conservé les identifiants de l’année précédente ou si vous avez interrompu par mégarde l’importation).

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](img/import/capture7.png)

Une fois l'importation terminée, **vous devez impérativement récupérer les identifiants des utilisateurs au format PDF**.

![enregistrer_pdf](img/import/capture8.png)
