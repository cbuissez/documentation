# Créer un connecteur Éléa sur MonCollège

## Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône __Administration__ dans vos applications pour lancer la console.

![admin-icon](img/ent91/admin-icon.png)

## Créer un connecteur

1. Cliquez sur __Gérer les connecteurs__.

  ![console](img/ent91/console.png)

2. Puis en haut à droite sur __Créer un connecteur__.

   ![connecteur](img/ent91/connecteur.png)

## Paramétrer votre connecteur

Utilisez le tableau et la capture d'écran ci-dessous pour compléter les champs.

| Paramètre       | Valeur                                                       |
| --------------- | ------------------------------------------------------------ |
| URL de l'icône  | https://etampes.elea.ac-versailles.fr/theme/vital/pix/logo.png |
| Identifiant     | __elea__ suivi de votre __UAI/RNE__ (sans espace, sans majuscule) |
| Nom d'affichage | Éléa                                                         |
| URL             | https://etampes.elea.ac-versailles.fr/login/index.php?authCAS=ENT91 |
| Cible           | Nouvelle page                                                |

__ATTENTION__ : Ces deux liens sont valables uniquement pour les établissements du bassin d'Étampes. Vous devez les adapter à votre situation en vous aidant du tableau en annexe !



![admin-cas-params](img/ent91/parametres.png)

Cliquez enfin sur le bouton **Créer**.

![Bouton Créer](img/ent91/boutoncreerconnecteur.png)

Remarque : si vous apportez ultérieurement des modifications sur cette page, cliquez alors en haut à droite sur __Enregistrer__.

![save](img/ent91/save.png)

## Activer les droits

Vous devez maintenant décider qui pourra accéder à ce connecteur. Rendez-vous dans **Attribution**.

![droits](img/ent91/droits.png)

Vous devriez donner l'accès aux profils suivants :

* Tous les enseignants
* Tous les élèves
* Tous les personnels

*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices ;-)*

## Annexe - adresse URL de chaque bassin

| Nom       | Adresse                                 |
| --------- | --------------------------------------- |
| Étampes   | https://etampes.elea.ac-versailles.fr   |
| Évry      | https://evry.elea.ac-versailles.fr      |
| Massy     | https://massy.elea.ac-versailles.fr     |
| Montgeron | https://montgeron.elea.ac-versailles.fr |
| Savigny   | https://savigny.elea.ac-versailles.fr   |
