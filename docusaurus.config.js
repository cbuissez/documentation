// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'ELEA Doc',
  tagline: 'Documentation pour l\'utilisation d\'ELEA',
  favicon: 'img/elea.ico',

  // Set the production url of your site here
  url: 'https://dne-elearning.gitlab.io/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/moodle-elea/documentation/',

    onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/elea.png',
      navbar: {
        title: 'ELEA doc',
        logo: {
          alt: 'logo ELEA',
          src: 'img/elea.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'Professeurs',
            position: 'left',
            label: 'Professeurs',
          },
          {
            type: 'doc',
            docId: 'Referents',
            position: 'left',
            label: 'Référents',
          },
          {
            type: 'doc',
            docId: 'Formateurs',
            position: 'left',
            label: 'Formateurs',
          },
          {
            type: 'doc',
            docId: 'Conseillers',
            position: 'left',
            label: 'Conseillers',
          },
          {
            type: 'doc',
            docId: 'FAQ',
            label: '❓FAQ',
            position: 'right',
          },
        ],
      },
      algolia: {
        // The application ID provided by Algolia
        appId: 'D8X21ZDUOW',
  
        // Public API key: it is safe to commit it
        apiKey: 'bcddf88fcc7d6c7fb89990dc4d1e3fd2',
  
        indexName: 'dne-elearning-gitlab',
  
        // Optional: see doc section below
        contextualSearch: true,
  
        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        externalUrlRegex: 'external\\.com|domain\\.com',
  
        // Optional: Replace parts of the item URLs from Algolia. Useful when using the same search index for multiple deployments using a different baseUrl. You can use regexp or string in the `from` param. For example: localhost:3000 vs myCompany.com/docs
        // replaceSearchResultPathname: {
        //  from: '/docs/', // or as RegExp: /\/docs\//
        //  to: '/',
        //},
  
        // Optional: Algolia search parameters
        searchParameters: {},
  
        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: 'search',
  
        //... other Algolia params
      },
      footer: {
        style: 'dark',
        links: [
           {
            title: 'Communautés',
            items: [
              {
                label: 'Elea sur Ac-Versailles',
                href: 'https://ressources.dane.ac-versailles.fr/ressource/elea',
              },
              {
                label: 'Communauté ELEA',
                href: 'https://communaute.elea.ac-versailles.fr/',
              },
            ],
          },
          {
            title: 'Mais aussi',
            items: [
              {
                label: 'IAM',
                href: 'https://iam.unistra.fr',
              },
              {
                label: 'Espace M@gistère',
                href: 'https://magistere.education.fr/ac-versailles/course/view.php?id=16570',
              },
            ],
          },
        ],
        copyright: `Equipe Elea Nationale, MENJ`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
