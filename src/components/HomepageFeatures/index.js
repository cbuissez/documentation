import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Un moodle adapté',
    Svg: require('@site/static/img/elea.png').default,
    description: (
      <>
        Une plateforme moodle adaptée à l'enseignement dans le second degré
      </>
    ),
  },
  {
    title: 'Des activités variées',
    Svg: require('@site/static/img/mode-de-vie.png').default,
    description: (
      <>
      Différents types d'activités pour favoriser l'autonomie et la collaboration
      </>
    ),
  },
  {
    title: 'Accompagner les élèves',
    Svg: require('@site/static/img/evaluation.png').default,
    description: (
      <>
        La plateforme permet de suivre le travail des élèves et propose des parcours personnalisés.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img src={Svg}/>
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
